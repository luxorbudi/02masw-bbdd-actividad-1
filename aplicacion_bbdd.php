<html>
	<head>
		<style type="text/css">
			#proyecto {
				font-family: Arial, Helvetica, sans-serif;
				border-collapse: collapse;
			}

			#proyecto td, #proyecto th {
				border: 1px solid #ddd;
				padding: 8px;
			}

			#proyecto tr:nth-child(even){background-color: #f2f2f2;}

			#proyecto tr:hover {background-color: #ddd;}

			#proyecto th {
				padding-top: 12px;
				padding-bottom: 12px;
				text-align: left;
				background-color: #04AA6D;
				color: white;
			}
		</style>
	</head>
	<body>
		<?php
			// ------------ CONEXIÓN ------------

				// Datos de la cadena de conexión

				// Conexión ElephantSQL
				$maquina = "HOST";

				// Puerto de la conexión
				$puerto = "5432";

				// Base de datos
				$bbdd = "BASE_DATOS";

				// Usuario de la base de datos
				$usuario = "USUARIO";

				// Contraseña de la base de datos
				$contrasenya = "CONTRASENIA";

				// Establecer la conexión a la BBDD PostgreSQL - ElephantSQL
				$conexion = pg_connect("host=$maquina port=$puerto dbname=$bbdd user=$usuario password=$contrasenya");

				// Estado de la conexión
				if(!$conexion) {
					echo utf8_encode("Error: No se ha podido realizar la conexión.");
					exit;
				}

			// ------------ CONEXIÓN ------------


			// ------------ CONSULTAS ------------

				function consulta_join1($conexion) {
					// Realizar una consulta SQL

					$consulta = "
						SELECT DISTINCT P.Titulo, E.NombreEquipo, U.Nombre || ' ' || U.Apellidos AS NombreCompleto, U.Telefono, U.Email
						  FROM \"02MASW-Schema\".USUARIO U
							INNER JOIN \"02MASW-Schema\".MIEMBRO_EQUIPO ME ON U.Id = ME.IdUsuario
							INNER JOIN \"02MASW-Schema\".EQUIPO E ON ME.IdEquipo = E.Id
							INNER JOIN \"02MASW-Schema\".PROYECTO P ON E.IdProyecto = P.Id";

					$resultado = pg_query($conexion, $consulta);

					// Comprobar resultado de la consulta
					if (!$resultado) {
						echo utf8_encode("No se ha podido realizar la consulta join 1, error: ").pg_last_error();
						exit;
					}

					// Comprobar que devuelve la consulta
					if (pg_num_rows($resultado) == 0) { // La consulta no tiene ninguna fila
						echo utf8_encode("No se encontró ninguna fila que coincida con la consulta join 1.");
					}
					else {
						echo "<p>======================================================================================<br/>";

						echo "CONSULTA JOIN 1 - CONSULTA DE MIEMBROS DE EQUIPOS Y LOS PROYECTOS A LOS QUE PERTENECEN<br/>";

						echo "======================================================================================<p/>";

						echo "<p>";

						// Mostrar los datos de la consulta

						echo "<table id='proyecto'>";
							echo "<thead align='left' style='display: table-header-group'>";
								echo "<tr>";
									echo "<th>PROYECTO</th>";
									echo "<th>EQUIPO</th>";
									echo "<th>NOMBRE COMPLETO</th>";
									echo "<th>TELEFONO</th>";
									echo "<th>EMAIL</th>";
								echo "</tr>";
							echo "</thead>";
							echo "<tbody>";
								while ($fila = pg_fetch_row($resultado)) {

									echo "<tr class='item_row'>";

										echo "<td> $fila[0] </td>";
										echo "<td> $fila[1] </td>";
										echo "<td> $fila[2] </td>";
										echo "<td> $fila[3] </td>";
										echo "<td> $fila[4] </td>";
									
									echo "</tr>";
								}
							echo "</tbody>";
						echo "</table>";
					}

					// Liberar los resultados
					pg_free_result($resultado);
				}

				// Realizar consultajoin1
				consulta_join1($conexion);




				function consulta_join2($conexion) {
					// Realizar una consulta SQL

					$consulta = "
						SELECT P.Titulo, E.NombreEquipo, A.NombreActividad, AR.Titulo
						  FROM \"02MASW-Schema\".PROYECTO P
							INNER JOIN \"02MASW-Schema\".EQUIPO E ON E.IdProyecto = P.Id
							INNER JOIN \"02MASW-Schema\".ACTIVIDAD A ON A.IdProyecto = P.Id
							INNER JOIN \"02MASW-Schema\".ARTICULO AR ON AR.IdActividad = A.Id
						ORDER BY P.Id, A.Id";

					$resultado = pg_query($conexion, $consulta);

					// Comprobar resultado de la consulta
					if (!$resultado) {
						echo utf8_encode("No se ha podido realizar la consulta join 2, error: ").pg_last_error();
						exit;
					}

					// Comprobar que devuelve la consulta
					if (pg_num_rows($resultado) == 0) { // La consulta no tiene ninguna fila
						echo utf8_encode("No se encontró ninguna fila que coincida con la consulta join 2.");
					}
					else {
						echo "<p>=========================================================================================<br/>";

						echo "CONSULTA JOIN 2 - CONSULTA DE ARTICULOS Y LOS PROYECTOS Y ACTIVIDADES A LOS QUE PERTENECEN<br/>";

						echo "=========================================================================================<p/>";

						echo "<p>";

						// Mostrar los datos de la consulta

						echo "<table id='proyecto'>";
							echo "<thead align='left' style='display: table-header-group'>";
								echo "<tr>";
									echo "<th>PROYECTO</th>";
									echo "<th>EQUIPO</th>";
									echo "<th>ACTIVIDAD</th>";
									echo "<th>ARTICULO</th>";
								echo "</tr>";
							echo "</thead>";
							echo "<tbody>";
								while ($fila = pg_fetch_row($resultado)) {

									echo "<tr class='item_row'>";

										echo "<td> $fila[0] </td>";
										echo "<td> $fila[1] </td>";
										echo "<td> $fila[2] </td>";
										echo "<td> $fila[3] </td>";

									echo "</tr>";
								}
							echo "</tbody>";
						echo "</table>";
					}

					// Liberar los resultados
					pg_free_result($resultado);
				}

				// Realizar consultajoin1
				consulta_join2($conexion);




				function consulta_agregada1($conexion) {
					$consulta = "
						SELECT P.Titulo, COUNT (AR.*)
							FROM \"02MASW-Schema\".PROYECTO P
								INNER JOIN \"02MASW-Schema\".ACTIVIDAD A ON A.IdProyecto = P.Id
								INNER JOIN \"02MASW-Schema\".ARTICULO AR ON AR.IdActividad = A.Id
							GROUP BY P.Id ORDER BY P.Id";

					$resultado = pg_query($conexion, $consulta);

					// Comprobar resultado de la consulta
					if (!$resultado) {
						echo utf8_encode("No se ha podido realizar la consulta agregada 1, error: ").pg_last_error();
						exit;
					}

					// Comprobar que devuelve la consulta
					if (pg_num_rows($resultado) == 0) { // La consulta no tiene ninguna fila
						echo utf8_encode("No se encontró ninguna fila que coincida con la consulta agregada 1.");
					}
					else {
						echo "<p>==========================================================<br/>";

						echo "CONSULTA AGREGADA 1 - CANTIDAD DE ARTICULOS POR PROYECTO<br/>";

						echo "==========================================================<p/>";

						echo "<p>";

						// Mostrar los datos de la consulta

						echo "<table id='proyecto'>";
							echo "<thead align='left' style='display: table-header-group'>";
								echo "<tr>";
									echo "<th colspan='2'>PROYECTO</th>";
								echo "</tr>";
							echo "</thead>";
							echo "<tbody>";
								while ($fila = pg_fetch_row($resultado)) {

									echo "<tr class='item_row'>";
										echo "<td> $fila[0] </td>";
										echo "<td> $fila[1] art&#237;culos</td>";
									
									echo "</tr>";
								}
							echo "</tbody>";
						echo "</table>";
					}

					// Liberar los resultados
					pg_free_result($resultado);
				}

				// Realizar consultajoin1
				consulta_agregada1($conexion);




				function consulta_agregada2($conexion) {
					$consulta = "
						SELECT P.Titulo, A.\"CantidadEquipos\", B.\"CantidadActividades\"
							FROM \"02MASW-Schema\".PROYECTO P,
								(SELECT P.Id, COUNT (E.*) AS \"CantidadEquipos\"
									FROM \"02MASW-Schema\".PROYECTO P
										INNER JOIN \"02MASW-Schema\".EQUIPO E ON E.IdProyecto = P.Id
									GROUP BY P.Id) A, 
								(SELECT P.Id, COUNT (C.*) AS \"CantidadActividades\"
									FROM \"02MASW-Schema\".PROYECTO P
										INNER JOIN \"02MASW-Schema\".ACTIVIDAD C ON C.IdProyecto = P.Id
									GROUP BY P.Id) B
							WHERE P.Id = A.Id AND P.Id = B.Id";

					$resultado = pg_query($conexion, $consulta);

					// Comprobar resultado de la consulta
					if (!$resultado) {
						echo utf8_encode("No se ha podido realizar la consulta agregada 2, error: ").pg_last_error();
						exit;
					}

					// Comprobar que devuelve la consulta
					if (pg_num_rows($resultado) == 0) { // La consulta no tiene ninguna fila
						echo utf8_encode("No se encontró ninguna fila que coincida con la consulta agregada 2.");
					}
					else {
						echo "<p>======================================================================<br/>";

						echo "CONSULTA AGREGADA 2 - CANTIDAD DE EQUIPOS Y ACTIVIDADES POR PROYECTO<br/>";

						echo "======================================================================<p/>";

						echo "<p>";

						// Mostrar los datos de la consulta

						echo "<table id='proyecto'>";
							echo "<thead align='left' style='display: table-header-group'>";
								echo "<tr>";
									echo "<th>PROYECTO</th>";
									echo "<th>CANTIDAD DE EQUIPOS</th>";
									echo "<th>CANTIDAD DE ACTIVIDADES</th>";
								echo "</tr>";
							echo "</thead>";
							echo "<tbody>";
								while ($fila = pg_fetch_row($resultado)) {

									echo "<tr class='item_row'>";
										echo "<td> $fila[0] </td>";
										echo "<td> $fila[1] </td>";
										echo "<td> $fila[2] </td>";
									echo "</tr>";
								}
							echo "</tbody>";
						echo "</table>";
					}

					// Liberar los resultados
					pg_free_result($resultado);
				}

				// Realizar consultajoin1
				consulta_agregada2($conexion);

			// ------------ CONSULTAS ------------
		?>
	</body>
</html>