INSERT INTO "02MASW-Schema".AREA_DEPORTIVA(Nombre, Descripcion) VALUES ('Fútbol', 'Área deportiva para todo lo relacionado al fútbol');
INSERT INTO "02MASW-Schema".AREA_DEPORTIVA(Nombre, Descripcion) VALUES ('Voleibol', 'Área deportiva para todo lo relacionado al voleibol');
INSERT INTO "02MASW-Schema".AREA_DEPORTIVA(Nombre, Descripcion) VALUES ('Baloncesto', 'Área deportiva para todo lo relacionado al baloncesto');
INSERT INTO "02MASW-Schema".AREA_DEPORTIVA(Nombre, Descripcion) VALUES ('Tenis', 'Área deportiva para todo lo relacionado al tenis');

INSERT INTO "02MASW-Schema".USUARIO(IdAreaDeportiva, Nombre, Apellidos, Telefono, Email, FechaNacimiento, Genero) VALUES (1, 'Maria', 'Vega Perez', 947748704, 'maria.vega.perez@xmail.com', '1994/04/12', 'F');
INSERT INTO "02MASW-Schema".USUARIO(IdAreaDeportiva, Nombre, Apellidos, Telefono, Email, FechaNacimiento, Genero) VALUES (1, 'Carmen', 'Duran Diaz', 990997930, 'carmen.duran.diaz@xmail.com', '1988/10/15', 'F');
INSERT INTO "02MASW-Schema".USUARIO(IdAreaDeportiva, Nombre, Apellidos, Telefono, Email, FechaNacimiento, Genero) VALUES (4, 'Ana', 'Suarez Rodriguez', 923892035, 'ana.suarez.rodriguez@xmail.com', '1989/10/18', 'F');
INSERT INTO "02MASW-Schema".USUARIO(IdAreaDeportiva, Nombre, Apellidos, Telefono, Email, FechaNacimiento, Genero) VALUES (3, 'Isabel', 'Garcia Castillo', 987913712, 'isabel.garcia.castillo@xmail.com', '1992/03/26', 'F');
INSERT INTO "02MASW-Schema".USUARIO(IdAreaDeportiva, Nombre, Apellidos, Telefono, Email, FechaNacimiento, Genero) VALUES (1, 'Dolores', 'Romero Ortiz', 936520432, 'dolores.romero.ortiz@xmail.com', '1992/11/06', 'F');
INSERT INTO "02MASW-Schema".USUARIO(IdAreaDeportiva, Nombre, Apellidos, Telefono, Email, FechaNacimiento, Genero) VALUES (2, 'Pilar', 'Cruz Marquez', 955991772, 'pilar.cruz.marquez@xmail.com', '1990/11/25', 'F');
INSERT INTO "02MASW-Schema".USUARIO(IdAreaDeportiva, Nombre, Apellidos, Telefono, Email, FechaNacimiento, Genero) VALUES (2, 'Teresa', 'Marin Gallego', 963664852, 'teresa.marin.gallego@xmail.com', '1991/12/01', 'F');
INSERT INTO "02MASW-Schema".USUARIO(IdAreaDeportiva, Nombre, Apellidos, Telefono, Email, FechaNacimiento, Genero) VALUES (1, 'Rosa', 'Ortiz Cano', 976672806, 'rosa.ortiz.cano@xmail.com', '1987/12/12', 'F');
INSERT INTO "02MASW-Schema".USUARIO(IdAreaDeportiva, Nombre, Apellidos, Telefono, Email, FechaNacimiento, Genero) VALUES (3, 'Josefa', 'Ramirez Alonso', 959878781, 'josefa.ramirez.alonso@xmail.com', '1985/09/14', 'F');
INSERT INTO "02MASW-Schema".USUARIO(IdAreaDeportiva, Nombre, Apellidos, Telefono, Email, FechaNacimiento, Genero) VALUES (1, 'Cristina', 'Molina Cortes', 953008420, 'cristina.molina.cortes@xmail.com', '1994/10/04', 'F');
INSERT INTO "02MASW-Schema".USUARIO(IdAreaDeportiva, Nombre, Apellidos, Telefono, Email, FechaNacimiento, Genero) VALUES (2, 'Angeles', 'Vicente Arias', 951433403, 'angeles.vicente.arias@xmail.com', '1991/10/24', 'F');
INSERT INTO "02MASW-Schema".USUARIO(IdAreaDeportiva, Nombre, Apellidos, Telefono, Email, FechaNacimiento, Genero) VALUES (2, 'Laura', 'Vidal Diez', 955252481, 'laura.vidal.diez@xmail.com', '1993/03/02', 'F');
INSERT INTO "02MASW-Schema".USUARIO(IdAreaDeportiva, Nombre, Apellidos, Telefono, Email, FechaNacimiento, Genero) VALUES (3, 'Antonia', 'Martinez Rubio', 990722263, 'antonia.martinez.rubio@xmail.com', '1992/08/20', 'F');
INSERT INTO "02MASW-Schema".USUARIO(IdAreaDeportiva, Nombre, Apellidos, Telefono, Email, FechaNacimiento, Genero) VALUES (1, 'Elena', 'Montero Aguilar', 979997950, 'elena.montero.aguilar@xmail.com', '1992/06/02', 'F');
INSERT INTO "02MASW-Schema".USUARIO(IdAreaDeportiva, Nombre, Apellidos, Telefono, Email, FechaNacimiento, Genero) VALUES (4, 'Marta', 'Caballero Bernui', 922957637, 'marta.caballero.bernui@xmail.com', '1993/06/06', 'F');
INSERT INTO "02MASW-Schema".USUARIO(IdAreaDeportiva, Nombre, Apellidos, Telefono, Email, FechaNacimiento, Genero) VALUES (1, 'Lucia', 'Reyes Nieto', 966281235, 'lucia.reyes.nieto@xmail.com', '1994/01/30', 'F');
INSERT INTO "02MASW-Schema".USUARIO(IdAreaDeportiva, Nombre, Apellidos, Telefono, Email, FechaNacimiento, Genero) VALUES (4, 'Francisca', 'Serrano Vargas', 952367028, 'francisca.serrano.vargas@xmail.com', '1987/11/24', 'F');
INSERT INTO "02MASW-Schema".USUARIO(IdAreaDeportiva, Nombre, Apellidos, Telefono, Email, FechaNacimiento, Genero) VALUES (1, 'Mercedes', 'Medina Medina', 990400270, 'mercedes.medina.medina@xmail.com', '1987/10/23', 'F');
INSERT INTO "02MASW-Schema".USUARIO(IdAreaDeportiva, Nombre, Apellidos, Telefono, Email, FechaNacimiento, Genero) VALUES (1, 'Luisa', 'Morales Roman', 989914789, 'luisa.morales.roman@xmail.com', '1988/02/07', 'F');
INSERT INTO "02MASW-Schema".USUARIO(IdAreaDeportiva, Nombre, Apellidos, Telefono, Email, FechaNacimiento, Genero) VALUES (2, 'Concepcion', 'Lozano Pascual', 949346206, 'concepcion.lozano.pascual@xmail.com', '1991/09/16', 'F');
INSERT INTO "02MASW-Schema".USUARIO(IdAreaDeportiva, Nombre, Apellidos, Telefono, Email, FechaNacimiento, Genero) VALUES (3, 'Rosario', 'Cortes Ferrer', 930462855, 'rosario.cortes.ferrer@xmail.com', '1991/11/16', 'F');
INSERT INTO "02MASW-Schema".USUARIO(IdAreaDeportiva, Nombre, Apellidos, Telefono, Email, FechaNacimiento, Genero) VALUES (4, 'Silvia', 'Martin Ibañez', 915267047, 'silvia.martin.ibañez@xmail.com', '1985/01/13', 'F');
INSERT INTO "02MASW-Schema".USUARIO(IdAreaDeportiva, Nombre, Apellidos, Telefono, Email, FechaNacimiento, Genero) VALUES (1, 'Paula', 'Sanchez Cabrera', 901948910, 'paula.sanchez.cabrera@xmail.com', '1989/02/17', 'F');
INSERT INTO "02MASW-Schema".USUARIO(IdAreaDeportiva, Nombre, Apellidos, Telefono, Email, FechaNacimiento, Genero) VALUES (3, 'Sara', 'Ferrer Martin', 934192698, 'sara.ferrer.martin@xmail.com', '1994/04/07', 'F');
INSERT INTO "02MASW-Schema".USUARIO(IdAreaDeportiva, Nombre, Apellidos, Telefono, Email, FechaNacimiento, Genero) VALUES (2, 'Raquel', 'Nuñez Gonzalez', 954610405, 'raquel.nuñez.gonzalez@xmail.com', '1988/12/04', 'F');
INSERT INTO "02MASW-Schema".USUARIO(IdAreaDeportiva, Nombre, Apellidos, Telefono, Email, FechaNacimiento, Genero) VALUES (4, 'Juana', 'Moreno Fuentes', 903543324, 'juana.moreno.fuentes@xmail.com', '1987/09/30', 'F');
INSERT INTO "02MASW-Schema".USUARIO(IdAreaDeportiva, Nombre, Apellidos, Telefono, Email, FechaNacimiento, Genero) VALUES (2, 'Rocio', 'Esteban Parra', 912475109, 'rocio.esteban.parra@xmail.com', '1991/01/12', 'F');
INSERT INTO "02MASW-Schema".USUARIO(IdAreaDeportiva, Nombre, Apellidos, Telefono, Email, FechaNacimiento, Genero) VALUES (4, 'Eva', 'Blanco Peña', 917855630, 'eva.blanco.peña@xmail.com', '1992/06/05', 'F');
INSERT INTO "02MASW-Schema".USUARIO(IdAreaDeportiva, Nombre, Apellidos, Telefono, Email, FechaNacimiento, Genero) VALUES (4, 'Manuela', 'Pascual Soto', 903003818, 'manuela.pascual.soto@xmail.com', '1994/07/24', 'F');
INSERT INTO "02MASW-Schema".USUARIO(IdAreaDeportiva, Nombre, Apellidos, Telefono, Email, FechaNacimiento, Genero) VALUES (3, 'Beatriz', 'Gonzalez Herrero', 935930292, 'beatriz.gonzalez.herrero@xmail.com', '1986/08/28', 'F');
INSERT INTO "02MASW-Schema".USUARIO(IdAreaDeportiva, Nombre, Apellidos, Telefono, Email, FechaNacimiento, Genero) VALUES (3, 'Patricia', 'Santos Gomez', 985930213, 'patricia.santos.gomez@xmail.com', '1988/10/30', 'F');
INSERT INTO "02MASW-Schema".USUARIO(IdAreaDeportiva, Nombre, Apellidos, Telefono, Email, FechaNacimiento, Genero) VALUES (2, 'Victoria', 'Flores Vidal', 999838188, 'victoria.flores.vidal@xmail.com', '1991/04/24', 'F');
INSERT INTO "02MASW-Schema".USUARIO(IdAreaDeportiva, Nombre, Apellidos, Telefono, Email, FechaNacimiento, Genero) VALUES (2, 'Jesus', 'Vazquez Iglesias', 924838878, 'jesus.vazquez.iglesias@xmail.com', '1988/02/02', 'F');
INSERT INTO "02MASW-Schema".USUARIO(IdAreaDeportiva, Nombre, Apellidos, Telefono, Email, FechaNacimiento, Genero) VALUES (3, 'Julia', 'Castillo Carrasco', 971431752, 'julia.castillo.carrasco@xmail.com', '1985/05/16', 'F');
INSERT INTO "02MASW-Schema".USUARIO(IdAreaDeportiva, Nombre, Apellidos, Telefono, Email, FechaNacimiento, Genero) VALUES (2, 'Andrea', 'Leon Leon', 986154146, 'andrea.leon.leon@xmail.com', '1989/11/23', 'F');
INSERT INTO "02MASW-Schema".USUARIO(IdAreaDeportiva, Nombre, Apellidos, Telefono, Email, FechaNacimiento, Genero) VALUES (1, 'Belen', 'Rubio Nuñez', 997072028, 'belen.rubio.nuñez@xmail.com', '1988/12/09', 'F');
INSERT INTO "02MASW-Schema".USUARIO(IdAreaDeportiva, Nombre, Apellidos, Telefono, Email, FechaNacimiento, Genero) VALUES (1, 'Silvia', 'Peña Campos', 991674658, 'silvia.peña.campos@xmail.com', '1989/02/04', 'F');
INSERT INTO "02MASW-Schema".USUARIO(IdAreaDeportiva, Nombre, Apellidos, Telefono, Email, FechaNacimiento, Genero) VALUES (1, 'Alba', 'Garrido Mendez', 966738385, 'alba.garrido.mendez@xmail.com', '1988/07/12', 'F');
INSERT INTO "02MASW-Schema".USUARIO(IdAreaDeportiva, Nombre, Apellidos, Telefono, Email, FechaNacimiento, Genero) VALUES (2, 'Esther', 'Soler Bravo', 965918107, 'esther.soler.bravo@xmail.com', '1989/09/08', 'F');
INSERT INTO "02MASW-Schema".USUARIO(IdAreaDeportiva, Nombre, Apellidos, Telefono, Email, FechaNacimiento, Genero) VALUES (3, 'Encarnacion', 'Mora Delgado', 901993735, 'encarnacion.mora.delgado@xmail.com', '1992/10/12', 'F');
INSERT INTO "02MASW-Schema".USUARIO(IdAreaDeportiva, Nombre, Apellidos, Telefono, Email, FechaNacimiento, Genero) VALUES (2, 'Nuria', 'Santana Montero', 934373265, 'nuria.santana.montero@xmail.com', '1990/01/19', 'F');
INSERT INTO "02MASW-Schema".USUARIO(IdAreaDeportiva, Nombre, Apellidos, Telefono, Email, FechaNacimiento, Genero) VALUES (4, 'Irene', 'Parra Duran', 956583325, 'irene.parra.duran@xmail.com', '1995/06/13', 'F');
INSERT INTO "02MASW-Schema".USUARIO(IdAreaDeportiva, Nombre, Apellidos, Telefono, Email, FechaNacimiento, Genero) VALUES (3, 'Montserrat', 'Marquez Muñoz', 957123491, 'montserrat.marquez.muñoz@xmail.com', '1990/06/20', 'F');
INSERT INTO "02MASW-Schema".USUARIO(IdAreaDeportiva, Nombre, Apellidos, Telefono, Email, FechaNacimiento, Genero) VALUES (3, 'Angela', 'Perez Navarro', 942487133, 'angela.perez.navarro@xmail.com', '1994/08/15', 'F');
INSERT INTO "02MASW-Schema".USUARIO(IdAreaDeportiva, Nombre, Apellidos, Telefono, Email, FechaNacimiento, Genero) VALUES (4, 'Sandra', 'Fernandez Flores', 983609115, 'sandra.fernandez.flores@xmail.com', '1994/01/23', 'F');
INSERT INTO "02MASW-Schema".USUARIO(IdAreaDeportiva, Nombre, Apellidos, Telefono, Email, FechaNacimiento, Genero) VALUES (3, 'Monica', 'Mendez Ruiz', 908664075, 'monica.mendez.ruiz@xmail.com', '1994/01/28', 'F');
INSERT INTO "02MASW-Schema".USUARIO(IdAreaDeportiva, Nombre, Apellidos, Telefono, Email, FechaNacimiento, Genero) VALUES (1, 'Inmaculada', 'Vargas Fernandez', 986163776, 'inmaculada.vargas.fernandez@xmail.com', '1986/01/25', 'F');
INSERT INTO "02MASW-Schema".USUARIO(IdAreaDeportiva, Nombre, Apellidos, Telefono, Email, FechaNacimiento, Genero) VALUES (1, 'Alicia', 'Gallego Ortega', 930104521, 'alicia.gallego.ortega@xmail.com', '1987/12/28', 'F');
INSERT INTO "02MASW-Schema".USUARIO(IdAreaDeportiva, Nombre, Apellidos, Telefono, Email, FechaNacimiento, Genero) VALUES (3, 'Yolanda', 'Ramos Cruz', 989502559, 'yolanda.ramos.cruz@xmail.com', '1986/02/11', 'F');
INSERT INTO "02MASW-Schema".USUARIO(IdAreaDeportiva, Nombre, Apellidos, Telefono, Email, FechaNacimiento, Genero) VALUES (3, 'Sonia', 'Nieto Esteban', 901713020, 'sonia.nieto.esteban@xmail.com', '1990/07/04', 'F');


INSERT INTO "02MASW-Schema".PROYECTO(IdUsuario, Titulo, Descripcion, FechaCreacion, FechaFinalizacion, FechaEliminacion) VALUES (1, 'La inclusión de las mujeres en el fútbol', 'La inclusión de las mujeres en los eventos deportivos de fútbol en la sociedad española', '2021/11/20', '2022/11/19', NULL);
INSERT INTO "02MASW-Schema".PROYECTO(IdUsuario, Titulo, Descripcion, FechaCreacion, FechaFinalizacion, FechaEliminacion) VALUES (6, 'Lanzamiento de la camiseta de la Selección Brasileña de Voleyball', 'Lanzamiento de la camiseta de la Selección Brasileña de Voleyball', '2021/08/16', '2022/11/08', NULL);
INSERT INTO "02MASW-Schema".PROYECTO(IdUsuario, Titulo, Descripcion, FechaCreacion, FechaFinalizacion, FechaEliminacion) VALUES (4, 'Experiencia de patrocinio Londres', 'Experiencia de patrocinio en Londres enmarcada en el Brasil Global Tour', '2021/09/02', '2022/10/23', NULL);
INSERT INTO "02MASW-Schema".PROYECTO(IdUsuario, Titulo, Descripcion, FechaCreacion, FechaFinalizacion, FechaEliminacion) VALUES (3, 'Semillero de futuras tenistas en España', 'El entrenamiento de futuras estrellas femeninas del tenis español', '2021/11/15', '2022/11/14', NULL);


INSERT INTO "02MASW-Schema".EQUIPO(IdProyecto, NombreEquipo, FechaCreacion) VALUES (1, 'Las futboleras', '2021/11/20');
INSERT INTO "02MASW-Schema".EQUIPO(IdProyecto, NombreEquipo, FechaCreacion) VALUES (2, 'Las camiseras', '2021/11/08');
INSERT INTO "02MASW-Schema".EQUIPO(IdProyecto, NombreEquipo, FechaCreacion) VALUES (3, 'Las leonas', '2021/10/23');
INSERT INTO "02MASW-Schema".EQUIPO(IdProyecto, NombreEquipo, FechaCreacion) VALUES (4, 'Las futuras estrellas', '2021/11/15');

INSERT INTO "02MASW-Schema".MIEMBRO_EQUIPO(IdUsuario, IdEquipo, FechaAdhesion) VALUES (1, 1, '2021/11/21');
INSERT INTO "02MASW-Schema".MIEMBRO_EQUIPO(IdUsuario, IdEquipo, FechaAdhesion) VALUES (2, 1, '2021/11/22');
INSERT INTO "02MASW-Schema".MIEMBRO_EQUIPO(IdUsuario, IdEquipo, FechaAdhesion) VALUES (5, 1, '2021/11/21');
INSERT INTO "02MASW-Schema".MIEMBRO_EQUIPO(IdUsuario, IdEquipo, FechaAdhesion) VALUES (8, 1, '2021/11/21');
INSERT INTO "02MASW-Schema".MIEMBRO_EQUIPO(IdUsuario, IdEquipo, FechaAdhesion) VALUES (10, 1, '2021/11/24');
INSERT INTO "02MASW-Schema".MIEMBRO_EQUIPO(IdUsuario, IdEquipo, FechaAdhesion) VALUES (14, 1, '2021/11/24');
INSERT INTO "02MASW-Schema".MIEMBRO_EQUIPO(IdUsuario, IdEquipo, FechaAdhesion) VALUES (16, 1, '2021/11/23');
INSERT INTO "02MASW-Schema".MIEMBRO_EQUIPO(IdUsuario, IdEquipo, FechaAdhesion) VALUES (18, 1, '2021/11/21');
INSERT INTO "02MASW-Schema".MIEMBRO_EQUIPO(IdUsuario, IdEquipo, FechaAdhesion) VALUES (19, 1, '2021/11/20');
INSERT INTO "02MASW-Schema".MIEMBRO_EQUIPO(IdUsuario, IdEquipo, FechaAdhesion) VALUES (23, 1, '2021/11/19');
INSERT INTO "02MASW-Schema".MIEMBRO_EQUIPO(IdUsuario, IdEquipo, FechaAdhesion) VALUES (36, 1, '2021/11/22');
INSERT INTO "02MASW-Schema".MIEMBRO_EQUIPO(IdUsuario, IdEquipo, FechaAdhesion) VALUES (37, 1, '2021/11/21');
INSERT INTO "02MASW-Schema".MIEMBRO_EQUIPO(IdUsuario, IdEquipo, FechaAdhesion) VALUES (38, 1, '2021/11/23');
INSERT INTO "02MASW-Schema".MIEMBRO_EQUIPO(IdUsuario, IdEquipo, FechaAdhesion) VALUES (47, 1, '2021/11/20');
INSERT INTO "02MASW-Schema".MIEMBRO_EQUIPO(IdUsuario, IdEquipo, FechaAdhesion) VALUES (48, 1, '2021/11/18');

INSERT INTO "02MASW-Schema".MIEMBRO_EQUIPO(IdUsuario, IdEquipo, FechaAdhesion) VALUES (6, 2, '2021/11/16');
INSERT INTO "02MASW-Schema".MIEMBRO_EQUIPO(IdUsuario, IdEquipo, FechaAdhesion) VALUES (7, 2, '2021/11/17');
INSERT INTO "02MASW-Schema".MIEMBRO_EQUIPO(IdUsuario, IdEquipo, FechaAdhesion) VALUES (11, 2, '2021/11/14');
INSERT INTO "02MASW-Schema".MIEMBRO_EQUIPO(IdUsuario, IdEquipo, FechaAdhesion) VALUES (12, 2, '2021/11/18');
INSERT INTO "02MASW-Schema".MIEMBRO_EQUIPO(IdUsuario, IdEquipo, FechaAdhesion) VALUES (20, 2, '2021/11/17');
INSERT INTO "02MASW-Schema".MIEMBRO_EQUIPO(IdUsuario, IdEquipo, FechaAdhesion) VALUES (25, 2, '2021/11/16');
INSERT INTO "02MASW-Schema".MIEMBRO_EQUIPO(IdUsuario, IdEquipo, FechaAdhesion) VALUES (27, 2, '2021/11/14');
INSERT INTO "02MASW-Schema".MIEMBRO_EQUIPO(IdUsuario, IdEquipo, FechaAdhesion) VALUES (32, 2, '2021/11/19');
INSERT INTO "02MASW-Schema".MIEMBRO_EQUIPO(IdUsuario, IdEquipo, FechaAdhesion) VALUES (33, 2, '2021/11/21');
INSERT INTO "02MASW-Schema".MIEMBRO_EQUIPO(IdUsuario, IdEquipo, FechaAdhesion) VALUES (35, 2, '2021/11/20');
INSERT INTO "02MASW-Schema".MIEMBRO_EQUIPO(IdUsuario, IdEquipo, FechaAdhesion) VALUES (39, 2, '2021/11/18');
INSERT INTO "02MASW-Schema".MIEMBRO_EQUIPO(IdUsuario, IdEquipo, FechaAdhesion) VALUES (41, 2, '2021/11/14');

INSERT INTO "02MASW-Schema".MIEMBRO_EQUIPO(IdUsuario, IdEquipo, FechaAdhesion) VALUES (4, 3, '2021/11/02');
INSERT INTO "02MASW-Schema".MIEMBRO_EQUIPO(IdUsuario, IdEquipo, FechaAdhesion) VALUES (9, 3, '2021/11/03');
INSERT INTO "02MASW-Schema".MIEMBRO_EQUIPO(IdUsuario, IdEquipo, FechaAdhesion) VALUES (13, 3, '2021/11/03');
INSERT INTO "02MASW-Schema".MIEMBRO_EQUIPO(IdUsuario, IdEquipo, FechaAdhesion) VALUES (21, 3, '2021/11/03');
INSERT INTO "02MASW-Schema".MIEMBRO_EQUIPO(IdUsuario, IdEquipo, FechaAdhesion) VALUES (24, 3, '2021/11/03');
INSERT INTO "02MASW-Schema".MIEMBRO_EQUIPO(IdUsuario, IdEquipo, FechaAdhesion) VALUES (30, 3, '2021/11/03');
INSERT INTO "02MASW-Schema".MIEMBRO_EQUIPO(IdUsuario, IdEquipo, FechaAdhesion) VALUES (31, 3, '2021/11/03');
INSERT INTO "02MASW-Schema".MIEMBRO_EQUIPO(IdUsuario, IdEquipo, FechaAdhesion) VALUES (34, 3, '2021/11/03');
INSERT INTO "02MASW-Schema".MIEMBRO_EQUIPO(IdUsuario, IdEquipo, FechaAdhesion) VALUES (40, 3, '2021/11/03');
INSERT INTO "02MASW-Schema".MIEMBRO_EQUIPO(IdUsuario, IdEquipo, FechaAdhesion) VALUES (43, 3, '2021/11/03');
INSERT INTO "02MASW-Schema".MIEMBRO_EQUIPO(IdUsuario, IdEquipo, FechaAdhesion) VALUES (44, 3, '2021/11/03');
INSERT INTO "02MASW-Schema".MIEMBRO_EQUIPO(IdUsuario, IdEquipo, FechaAdhesion) VALUES (46, 3, '2021/11/03');
INSERT INTO "02MASW-Schema".MIEMBRO_EQUIPO(IdUsuario, IdEquipo, FechaAdhesion) VALUES (49, 3, '2021/11/03');
INSERT INTO "02MASW-Schema".MIEMBRO_EQUIPO(IdUsuario, IdEquipo, FechaAdhesion) VALUES (50, 3, '2021/11/03');

INSERT INTO "02MASW-Schema".MIEMBRO_EQUIPO(IdUsuario, IdEquipo, FechaAdhesion) VALUES (3, 4, '2021/11/03');
INSERT INTO "02MASW-Schema".MIEMBRO_EQUIPO(IdUsuario, IdEquipo, FechaAdhesion) VALUES (15, 4, '2021/11/03');
INSERT INTO "02MASW-Schema".MIEMBRO_EQUIPO(IdUsuario, IdEquipo, FechaAdhesion) VALUES (17, 4, '2021/11/03');
INSERT INTO "02MASW-Schema".MIEMBRO_EQUIPO(IdUsuario, IdEquipo, FechaAdhesion) VALUES (22, 4, '2021/11/03');
INSERT INTO "02MASW-Schema".MIEMBRO_EQUIPO(IdUsuario, IdEquipo, FechaAdhesion) VALUES (26, 4, '2021/11/03');
INSERT INTO "02MASW-Schema".MIEMBRO_EQUIPO(IdUsuario, IdEquipo, FechaAdhesion) VALUES (28, 4, '2021/11/03');
INSERT INTO "02MASW-Schema".MIEMBRO_EQUIPO(IdUsuario, IdEquipo, FechaAdhesion) VALUES (29, 4, '2021/11/03');
INSERT INTO "02MASW-Schema".MIEMBRO_EQUIPO(IdUsuario, IdEquipo, FechaAdhesion) VALUES (42, 4, '2021/11/03');
INSERT INTO "02MASW-Schema".MIEMBRO_EQUIPO(IdUsuario, IdEquipo, FechaAdhesion) VALUES (45, 4, '2021/11/03');


INSERT INTO "02MASW-Schema".ACTIVIDAD(IdProyecto, NombreActividad, FechaInicio, FechaFin) VALUES (1, 'Charla de concientización sobre el rol de las mujeres en el fútbol', '2021/11/24', '2021/11/25');
INSERT INTO "02MASW-Schema".ACTIVIDAD(IdProyecto, NombreActividad, FechaInicio, FechaFin) VALUES (1, 'Seminario sobre la historia de las mujeres en el fútbol español', '2021/11/27', '2021/11/30');
INSERT INTO "02MASW-Schema".ACTIVIDAD(IdProyecto, NombreActividad, FechaInicio, FechaFin) VALUES (1, 'Charla sobre las estrellas femeninas del fútbol español', '2021/11/26', '2021/11/26');

INSERT INTO "02MASW-Schema".ACTIVIDAD(IdProyecto, NombreActividad, FechaInicio, FechaFin) VALUES (2, 'Muestra camiseta de la Selección Brasileña de Voleyball', '2021/09/20', '2021/09/20');

INSERT INTO "02MASW-Schema".ACTIVIDAD(IdProyecto, NombreActividad, FechaInicio, FechaFin) VALUES (3, 'Charla sobre el patrocinio en Londres', '2021/10/01', '2021/10/01');
INSERT INTO "02MASW-Schema".ACTIVIDAD(IdProyecto, NombreActividad, FechaInicio, FechaFin) VALUES (3, '1er Campeonato infantil femenino de baloncesto interdistrital en la ciudad de Madrid', '2021/10/01', '2021/10/10');

INSERT INTO "02MASW-Schema".ACTIVIDAD(IdProyecto, NombreActividad, FechaInicio, FechaFin) VALUES (4, 'Seminario sobre cómo crear un semillero de futuras estrellas', '2021/11/18', '2021/12/05');
INSERT INTO "02MASW-Schema".ACTIVIDAD(IdProyecto, NombreActividad, FechaInicio, FechaFin) VALUES (4, 'Charla sobre la historia de las campeonas femeninas del tenis español', '2021/11/19', '2021/11/19');


INSERT INTO "02MASW-Schema".ARTICULO(IdActividad, Titulo, Contenido) VALUES (1, 'Concientización sobre el rol de las mujeres en el fútbol', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.');
INSERT INTO "02MASW-Schema".ARTICULO(IdActividad, Titulo, Contenido) VALUES (2, 'Historia de las mujeres en el fútbol español', 'Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.');
INSERT INTO "02MASW-Schema".ARTICULO(IdActividad, Titulo, Contenido) VALUES (2, 'Las estrellas femeninas del fútbol español', 'Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.');

INSERT INTO "02MASW-Schema".ARTICULO(IdActividad, Titulo, Contenido) VALUES (4, 'Detalles del evento para la presentación de la Selección Brasileña de Voleyball', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.');

INSERT INTO "02MASW-Schema".ARTICULO(IdActividad, Titulo, Contenido) VALUES (5, 'Charla sobre el patrocinio en Londres', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.');

INSERT INTO "02MASW-Schema".ARTICULO(IdActividad, Titulo, Contenido) VALUES (7, 'La creación de semilleros para futuras estrellas del tenis español', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.');
INSERT INTO "02MASW-Schema".ARTICULO(IdActividad, Titulo, Contenido) VALUES (8, 'La historia de las campeonas femeninas del tenis español', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.');